///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04c - Hello World
//
//
// Usage: 2nd Hello World Program in C++
//
//
// @author Vinton Sistoza <sistozav@wiliki>
// @date   2/11/2021
///////////////////////////////////////////////////////////////////////////////
#include <iostream>

using namespace std;

int main()
{
   string hello = string("Hello World\n");
   cout << hello;
   return 0;

}

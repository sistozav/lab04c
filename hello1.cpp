///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04c - Hello World
//
//
// Usage: Hello World Prog in C++
//
//
// @author Vinton Sistoza <sistozav@wiliki>
// @date   2/11/2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

using namespace std;

int main()
{
   cout << "Hello" << " " << "World!" << endl;
   return 0;
}
